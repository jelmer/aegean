aegean (0.15.2+dfsg-2) unstable; urgency=medium

  [ Andreas Tille ]
  * Fix and adjust upstream metadata.

  [ Steffen Moeller ]
  * Fix EDAM annotations.

  [ Sascha Steinbiss ]
  * Update Uploader email address.
  * Use debhelper 11.
  * Use secure d/copyright format link.
  * Bump Standards-Version.
  * Update Vcs-* fields with Salsa addresses.
  * Replace priority extra with optional.
  * Remove unnecessary Testsuite field.
  * Drop unused patch files.
  * Drop -dbg package.
  * Fix naming of lintian override.

 -- Sascha Steinbiss <satta@debian.org>  Wed, 04 Jul 2018 18:20:29 +0200

aegean (0.15.2+dfsg-1) unstable; urgency=medium

  * New upstream version.
  * Drop patches applied upstream.
  * Add override for pedantic Lintian msg.
  * d/watch: make watchfile more robust
  * d/rules: drop unneeded get-orig-source target
  * d/rules: enable full hardening
  * d/rules: use DEB_TARGET_ARCH_BITS
  * d/control: bump standards version

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sun, 20 Mar 2016 11:31:05 +0000

aegean (0.15.1+dfsg-3) unstable; urgency=low

  [ Jon Ison, Steffen Moeller, Sascha Steinbiss ]
  * Added EDAM annotation.

  [ Sascha Steinbiss ]
  * Migrate package to Git

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sun, 07 Feb 2016 08:55:37 +0000

aegean (0.15.1+dfsg-2) unstable; urgency=medium

  * Remove broken archs, as revealed by newly added tests.

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sat, 09 Jan 2016 18:28:59 +0000

aegean (0.15.1+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sat, 09 Jan 2016 14:16:31 +0000

aegean (0.14.1+dfsg2-2) unstable; urgency=low

  * d/rules: build w/o parallelism, set LC_ALL
  * patch Makefile to remove nondeterminism

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sun, 29 Nov 2015 19:19:01 +0000

aegean (0.14.1+dfsg2-1) unstable; urgency=medium

  * Exclude DataTables JS source, add Depends to new package
    Closes: #798900
  * Remove now redundant Lintian override

 -- Sascha Steinbiss <sascha@steinbiss.name>  Fri, 27 Nov 2015 22:35:59 +0000

aegean (0.14.1+dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sat, 10 Oct 2015 09:43:34 +0000

aegean (0.13.0+dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Sascha Steinbiss <sascha@steinbiss.name>  Tue, 28 Jul 2015 08:50:09 +0000

aegean (0.10.2+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #769162)

 -- Sascha Steinbiss <sascha@steinbiss.name>  Tue, 11 Nov 2014 11:14:11 +0000
